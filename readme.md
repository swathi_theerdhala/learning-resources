# Learning Resources

## Django Resources
Getting Started 1 - https://www.djangoproject.com/start/
Getting started 2 - http://www.fullstackpython.com/django.html
Django Turorial of creating simple page - https://www.youtube.com/playlist?list=PLEsfXFp6DpzTJ9L8KrHfF_vt9mSV5foAO
Django Official Documentation - https://docs.djangoproject.com/en/1.8/
Awesome Django Resources - https://github.com/rosarior/awesome-django

## Django Rest Framework
DjangoREST + AngularJS 1 [THE BEST & LIGHTEST] - http://blog.kevinastone.com/getting-started-with-django-rest-framework-and-angularjs.html
DjangoREST + AngularJS 2 - https://thinkster.io/django-angularjs-tutorial
DjangoREST + AngularJS 3 - http://mourafiq.com/2013/07/01/end-to-end-web-app-with-django-angular-1.html (https://github.com/mouradmourafiq/django-angular-blog)



## Implementing REDIS/ memcache
by heroku - https://devcenter.heroku.com/articles/django-memcache

## Implement SSL


## Deploying Django on EC2 AWS in Production mode
http://agiliq.com/blog/2014/08/deploying-a-django-app-on-amazon-ec2-instance/
Add Django server to a gunicorn worker - http://stackoverflow.com/questions/28598356/how-to-config-django-and-how-to-run-gunicorn-workers-with-cpu-performance


## Synchronous Payment Gateway (includes pay.feazt.com)


## Testing APIs
1. Runscope
2. APIary
3. Automating testing - https://realpython.com/blog/python/testing-in-django-part-1-best-practices-and-examples/
4. WebTests - https://pypi.python.org/pypi/django-webtest


## Add Permissions and Ownership to API (header Authorisation)
1. Writing Permission - http://blog.kevinastone.com/getting-started-with-django-rest-framework-and-angularjs.html#add-permissions-and-ownership-to-api


## API Integrations
1. PayU
2. PayTM

## Task Queing


## sockets io


## All SMS Implementation


## All Emails Implementation


## Best practices for writing Django RESTFul services


## Security in Django
https://docs.djangoproject.com/en/1.8/topics/security/
