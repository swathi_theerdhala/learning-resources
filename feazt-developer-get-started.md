# Feazt Developer - Get Started Checklist


1. Attitude Screening
2. Aptitude Screening


## Get Started with - 1st Week
Day 1:
1. Org Philosophy & Vision
2. Products (Web / Mobile Apps.. Eg: )
3. Individual Product Vision - What we aim to do this Product (Web/Mobile App)
4. Technical Stack
5. Expectations at Personal level ( what team expects from him/her).
6. Developer Rules
Day 2 - 6:
7. Understanding the Product - functional, design sessions.
(== test on day 6[5PM - 7PM] ==)

Lets plan some test or interview/viva regarding the 1st weeks learnings where his Understanding is tested and his Understanding capabilities are tested.
If any of the above are not clear to the developer one week after he/she is hired, the next step is to Fire him/her.

## Technical Training - 2nd Week
Day 7:
  1. Introduction to Django + MongoDB + Memcaching
  2. Introduction to AngularJS - MVC.
  3. Introduction to AWS (EC2, S3, LoadBalancers )
Day 8 - 12:
  4. Course work -
    4.1 - DanWahlin Introduction   to AngularJS in 60ish minutes
    4.2 - http://blog.ninja-squad.com/2015/07/21/what-is-new-angularjs-1.4/
    4.2 - Basic Understanding on concepts of scopes, controllers, directives, filters, factories, routes, templating, animations,
    4.3 - Basic understanding on concepts of inbuilt directives ng-repeat, ng-click
    4.3 - https://www.codecademy.com/courses/learn-angularjs

Test on the 12th day, then YES OR NO for the candidate.
